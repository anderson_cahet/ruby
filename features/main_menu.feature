Feature: As a user I want to convert Units
  @navigationmenu
  Scenario: When I tap on menu icon, I should see left side menu
    * I land on home screen
    When I press on menu icon
    Then I should see left side menu

  @convertionscreen
  Scenario: I should be able to open My conversions screen
    * I land on home screen
    * I press on menu icon
    And I press on My Conversion button
    Then I land on My Conversion screen


