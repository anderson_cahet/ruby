_favorites_icon_locator_ = "action_add_favorites"
_navigation_my_favorite_conversion_="Favorite conversions"
_favorite_conversion_empty_state_="text_info_favorites"
_favorite_conversions_populated_="favorites_single_line"

Then("Left unit picker should be foot") do
  puts("value on the left")
end

Then("Left unit picker should be {string}") do |leftPicker|

  puts("Left value is: ", leftPicker)
end

Then("right picker should be {string}") do |rightPicker|
  puts("centimeter on the reight", rightPicker)
end

Then(/^show all button should be (enabled|disabled)$/) do |state|
  if state == "enabled"
    puts("button is enabled")
  elsif state == "disabled"
    puts("button is disabled")
  end
end

When("I press on clear button") do
  puts("clear button pressed")
end

When("I type {string}") do |target|
  puts("I type: #{target}")
end

Then(/^I see "([^"]*)"$/) do |result|
  puts("result is #{result}")
end

Then(/^I press in favorites icon$/) do
  find_element(id:_favorites_icon_locator_).click
end

Then(/^I press on favorire conversion/) do
  text(_navigation_my_favorite_conversion_).click
end

Then(/^verify "([^"]*)" added to list$/) do |unit_type|
  text(unit_type)
end

