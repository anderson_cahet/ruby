_search_magnifier_locator_ = "action_search"
_favorites_icon_locator_ = "action_add_favorites"
_open_navigation_drawer_="Open navigation drawer"
_navigation_title_="Unit Converter"
_navigation_my_conversion_="My conversions"
_my_conversion_empty_state="text_info_no_custom_conversions"
_my_conversion_empty_state_button_="btn_new_custom_conversion"


Given("I land on home screen") do
  find_element(id:_search_magnifier_locator_)
  find_element(id:_favorites_icon_locator_)
  puts("I land on home screen")
end

When("I press on menu icon") do
  find_element(accessibility_id:_open_navigation_drawer_).click
  puts("menu pressed")
end

Then("I should see left side menu") do
  text(_navigation_title_)
  puts("I should see left side menu")
end

When("I press on My Conversion button") do
  text(_navigation_my_conversion_).click
  puts("My Conversions button pressed")
end

Then(/^I land on My Conversion screen$/) do
  find_element(id:_my_conversion_empty_state)
  find_element(id:_my_conversion_empty_state_button_)

  puts("I see My Conversions screen in empty state")

end
