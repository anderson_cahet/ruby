Feature: Tests for home screen functionality
  @default
  Scenario: Default values on home screen is foot and centimeter
    Given I land on home screen
    Then Left unit picker should be "Foot"
    Then Left unit picker should be "Inch"
    And right picker should be "Centimeter"

  Scenario: Show all button should be enabled at launch
    * I land on home screen
    Then show all button should be enabled
    When I press on clear button
    Then show all button should be disabled

    @conversions
  Scenario Outline: Verify default conversion
    * I land on home screen
    When I type "<target>"
    Then I see "<result>"
  Examples:
    |target|result|
    | 1    | 12   |
    | 2    | 24   |
    | 3    | 36   |
    | 4    | 186  |


  Scenario: Add conversion to favorites
    * I land on home screen
    And I press in favorites icon
    * I press on menu icon
    Then I press on favorire conversion
    And verify "length" added to list



