require 'appium_lib'

def appCaps
  { caps: {
      deviceName:"Android device",
      platformName: "Android",
      app: (File.join(File.dirname(__FILE__), "PreciseUnitConversion.apk")),
      appPackage:"com.ba.universalconverter",
      appActivity: "MainConverterActivity",
      newCommandTimeout: "3600"
  }}
end

Appium::Driver.new(appCaps, true)
Appium.promote_appium_methods Object